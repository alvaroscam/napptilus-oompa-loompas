import React, { Component } from 'react';
import Router from './app/Router';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import oompas from './app/reducers/OompasReducer';

class App extends Component {
  render() {
    return (
      <Provider store={createStore(oompas, {}, applyMiddleware(ReduxThunk))}>
        <Router />
      </Provider>
    );
  }
}

export default App;
