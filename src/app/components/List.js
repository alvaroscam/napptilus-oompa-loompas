import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchOompas } from '../actions/OompasActions';
import SingleOompa from './SingleOompa';
import { FormGroup, FormControl, Glyphicon } from 'react-bootstrap';
import InfiniteScroll from 'react-infinite-scroll-component';

class List extends Component {
  state = {
    oompas: [],
    filteredOompas: []
  };

  componentWillMount() {
    this.props.fetchOompas();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.oompas !== nextProps.oompas) {
      try {
        // Guardamos la lista que tenemos en local
        const TMP_LIST = localStorage.getItem('oompas_list');

        // Si no tenemos nada, lo seteamos
        if (TMP_LIST === null) {
          // No hay nada seteado, pues vamos a ello

          // Creamos y añadimos el objeto que vamos a meter en el storage
          const TMP_OBJECT = {
            list: nextProps.oompas,
            date: new Date()
          };

          localStorage.setItem('oompas_list', JSON.stringify(TMP_OBJECT));
          // Hasta aquí el objeto seteado
        } else {
          // Recuperamos la lista y la fecha de ésta
          const LIST = JSON.parse(TMP_LIST);
          const OOMPAS_LIST = LIST.list;
          const LIST_DATE = LIST.date;

          const NOW = new Date();
          const ONE_DAY = 86400;

          // Comparamos fechas, si ha pasado más de un día, recibiremos la lista de las props
          // Si no han pasado 24 horas, usaremos los datos guardados en local
          if (NOW - LIST_DATE > ONE_DAY) {
            console.log('Ha pasado más de un día, buscando una lista nueva...');
            this.setState({
              oompas: nextProps.oompas,
              filteredOompas: nextProps.oompas
            });
          } else {
            console.log('¡No ha pasado un día completo! Vamos a utilizar la lista local...');
            this.setState({
              oompas: OOMPAS_LIST,
              filteredOompas: OOMPAS_LIST
            });
          }
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

  onChange(text) {
    let filter = text.target.value;
    let filteredOompas = this.state.oompas;
    filteredOompas = filteredOompas.filter(oompa => {
      let oompaName = oompa.first_name.toLowerCase() + ' ' + oompa.last_name.toLowerCase();
      let oompaProfession = oompa.profession.toLowerCase();
      return oompaName.indexOf(filter.toLowerCase()) !== -1 || oompaProfession.indexOf(filter.toLowerCase()) !== -1;
    });
    this.setState({
      filteredOompas
    });
  }

  renderList() {
    return this.state.filteredOompas.map((oompa, i) => {
      return <SingleOompa key={i} {...oompa} />;
    });
  }

  fetchMoreOompas = () => {
    this.setState({
      filteredOompas: this.state.filteredOompas.concat(Array.from(this.state.filteredOompas))
    });
  };

  render() {
    return (
      <div align="center">
        <div align="right">
          <FormGroup style={styles.inputContainer}>
            <FormControl type="text" placeholder="Search" onChange={this.onChange.bind(this)} />
            <FormControl.Feedback>
              <Glyphicon glyph="search" />
            </FormControl.Feedback>
          </FormGroup>
        </div>
        <h1 style={styles.h1}>Find your Oompa Loompa</h1>
        <h2 style={styles.h2}>There are more than 100k</h2>
        <InfiniteScroll dataLength={this.state.filteredOompas.length} next={this.fetchMoreOompas} hasMore={true}>
          {this.renderList()}
        </InfiniteScroll>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  oompas: state.oompas
});

export default connect(
  mapStateToProps,
  { fetchOompas }
)(List);

const styles = {
  inputContainer: {
    width: 250,
    height: 40,
    margin: 10
  },
  input: {
    margin: 20,
    border: 'none',
    padding: 5
  },
  icon: {
    width: 15,
    height: 15
  },
  h1: {
    margin: 0
  },
  h2: {
    margin: 0,
    color: 'grey'
  }
};
