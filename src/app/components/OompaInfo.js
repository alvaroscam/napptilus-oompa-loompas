import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchOompa } from '../actions/OompasActions';

class OompaInfo extends Component {
  state = {
    oompa: []
  };

  componentWillMount() {
    const id = this.props.match.params.id;
    this.props.fetchOompa(id);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.oompa !== nextProps.oompa) {
      // Guardamos al oompa en un item que tenga de clave su email, para no traer siempre el mismo oompa
      const OOMPA_EMAIL = nextProps.oompa.email;

      let TMP_OOMPA = JSON.parse(localStorage.getItem(OOMPA_EMAIL));
      // Si no existe este oompa, lo creamos
      if (TMP_OOMPA === null) {
        // Al ser la primera vez, seteamos los datos de la API
        this.setState({
          oompa: nextProps.oompa
        });

        const TMP_OBJECT = {
          oompa: nextProps.oompa,
          date: new Date()
        };
        TMP_OOMPA = localStorage.setItem(OOMPA_EMAIL, JSON.stringify(TMP_OBJECT));
        // Si existe, pasamos a comprobar fechas
      } else {
        const OOMPA_INFO = TMP_OOMPA.oompa;
        const OOMPA_DATE = TMP_OOMPA.date;

        const NOW = new Date();
        const ONE_DAY = 86400;

        if (NOW - OOMPA_DATE > ONE_DAY) {
          console.log('Ha pasado más de un día, buscando datos nuevos...');
          this.setState({
            oompa: nextProps.oompa
          });
        } else {
          console.log('No ha pasado más de un día, vamos a utilizar los datos locales...');
          this.setState({
            oompa: OOMPA_INFO
          });
        }
      }
    }
  }

  renderOompa() {
    return (
      <div align="center" style={styles.container}>
        <div>
          <img src={this.state.oompa.image} alt={this.state.oompa.first_name} style={styles.image} />
        </div>
        <div style={styles.textContainer} align="left">
          <p style={styles.textName}>
            {this.state.oompa.first_name}&nbsp;{this.state.oompa.last_name}
          </p>
          <p style={styles.textGender}>{this.state.oompa.gender === 'M' ? 'Male' : 'Female'}</p>
          <p style={styles.textProfession}>{this.state.oompa.profession}</p>
          <p style={styles.textDescription} dangerouslySetInnerHTML={{ __html: this.state.oompa.description }} />
        </div>
      </div>
    );
  }

  render() {
    return this.renderOompa();
  }
}

const mapStateToProps = state => ({
  oompa: state.oompa
});

export default connect(
  mapStateToProps,
  { fetchOompa }
)(OompaInfo);

const styles = {
  container: {
    marginTop: 50,
    overflow: 'auto'
  },
  image: {
    height: 400,
    width: 500,
    float: 'left',
    display: 'block',
    marginLeft: 300
  },
  textContainer: {
    marginLeft: 810,
    marginRight: 340
  },
  textName: {
    fontWeight: 'bold',
    fontSize: 20,
    margin: 0
  },
  textGender: {
    color: 'grey',
    margin: 0
  },
  textProfession: {
    color: 'grey',
    margin: 0,
    fontStyle: 'italic'
  },
  textDescription: {
    marginTop: 20
  }
};
