import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Header extends Component {
  render() {
    return (
      <div style={styles.container}>
        <Link to="/">
          <img
            src="https://s3.eu-central-1.amazonaws.com/napptilus/level-test/imgs/logo-umpa-loompa.png"
            style={styles.image}
            alt="Oompas Logo"
          />
        </Link>
        <p style={styles.text}>Oompa Loompa's Crew</p>
      </div>
    );
  }
}

const styles = {
  container: {
    flexDirection: 'row',
    backgroundColor: '#dfe6e9',
    height: 40,
    margin: 0,
    alignContent: 'center',
    justifyContent: 'center',
    padding: 10
  },
  image: {
    height: 20,
    width: 25,
    marginLeft: 100,
    marginRight: 20,
    float: 'left'
  },
  text: {
    fontWeight: 'bold'
  }
};
