import React from 'react';
import { Link } from 'react-router-dom';

const SingleOompa = props => (
  <div className="col-md-4" style={styles.container}>
    <Link to={`/oompa/${props.id}`}>
      <img alt={props.first_name} src={props.image} style={styles.image} />
      <div style={styles.textContainer} align="left">
        <p style={styles.name}>
          {props.first_name}&nbsp;{props.last_name}
        </p>
        <p style={styles.gender}>{props.gender === 'M' ? 'Male' : 'Female'}</p>
        <p style={styles.profession}>{props.profession} </p>
      </div>
    </Link>
  </div>
);

export default SingleOompa;

const styles = {
  container: {
    marginTop: 20,
    textDecoration: 'none'
  },
  image: {
    height: 200,
    width: 300
  },
  textContainer: {
    width: 300
  },
  name: {
    fontWeight: 'bold',
    fontSize: 16,
    margin: 1,
    marginTop: 15
  },
  gender: {
    color: 'grey',
    margin: 1
  },
  profession: {
    color: 'grey',
    fontStyle: 'italic'
  }
};
