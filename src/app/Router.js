import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Page404 from './Page404';
import Header from './components/Header';
import List from './components/List';
import OompaInfo from './components/OompaInfo';

export default class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <div style={{ flex: 1 }}>
          <Header />
          <Switch>
            <Route exact path="/" component={List} />
            <Route strict path="/oompa/:id" component={OompaInfo} />
            <Route component={Page404} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}
