import { FETCH_OOMPAS, FETCH_OOMPA } from '../actions/types';

const oompas = (state = [], action) => {
  switch (action.type) {
    case FETCH_OOMPAS:
      return { ...state, oompas: action.payload };
    case FETCH_OOMPA:
      return { ...state, oompa: action.payload };
    default:
      return state;
  }
};

export default oompas;
