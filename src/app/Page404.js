import React, { Component } from 'react';

export default class Page404 extends Component {
  render() {
    return (
      <div>
        <p> 404: Oompa Loompas not found here! Maybe we should tell Johnny De... I mean, Willy! </p>
      </div>
    );
  }
}
