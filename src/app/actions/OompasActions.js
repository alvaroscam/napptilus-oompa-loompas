import axios from 'axios';
import { FETCH_OOMPAS, FETCH_OOMPA } from './types';

export const fetchOompas = () => {
  return dispatch => {
    axios
      .get('https://2q2woep105.execute-api.eu-west-1.amazonaws.com/napptilus/oompa-loompas')
      .then(res => {
        dispatch({ type: FETCH_OOMPAS, payload: res.data.results });
      })
      .catch(error => {
        console.log(error);
      });
  };
};

export const fetchOompa = id => {
  return dispatch => {
    axios
      .get(`https://2q2woep105.execute-api.eu-west-1.amazonaws.com/napptilus/oompa-loompas/${id}`)
      .then(res => {
        dispatch({ type: FETCH_OOMPA, payload: res.data });
      })
      .catch(error => {
        console.log(error);
      });
  };
};
