* To install the project and dependencies:
 	* Clone the project
  	* Run "npm install" or "yarn install"
  	* To start in dev mode run "npm start", to make a production build, run "npm build"
  	
	
* Framework:
  	* React
  	
	
* Libraries used:
  	* react-router: https://github.com/ReactTraining/react-router
  	* redux (along with react-redux and redux-thunk for async calls): https://github.com/reduxjs/redux
 	* axios: https://github.com/axios/axios
  	* react-bootstrap: https://react-bootstrap.github.io/
  	* react-infinite-scroll-component: https://www.npmjs.com/package/react-infinite-scroll-component